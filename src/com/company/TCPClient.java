package com.company;
/*
 * TCPDatanetworks - ${PACKAGE_NAME}
 * Created by Vaughn on 2-2-2018.
 */

import java.io.*;
import java.net.*;

class TCPClient {
    private Socket clientSocket = null;
    private DataOutputStream outToServer = null;
    private BufferedReader inFromServer = null;
    TCPClient(){
        while(true){
            try {
                clientSocket = new Socket(Session.getInstance().getHost(), Session.getInstance().getPort());
                outToServer = new DataOutputStream(clientSocket.getOutputStream());
                inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                break;
                //System.out.println("Server ready to receive established on " + clientSocket.getInetAddress().toString() + ":" + clientSocket.getPort() + "!");
            } catch (IOException e) {
                System.out.println("Error: could establish connection to server, retrying in 1 second.");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }

    }
    public String readLine(){
        try {
            String line = inFromServer.readLine();
            if(line.contains("token>")){
                Session.getInstance().setKey(line.substring(line.indexOf('>') + 1, line.length()));
                System.out.println("S:210 Logged in!");
            } else {
                line = line.replace(" ---","\n");
                System.out.println("S:"+line);
            }
            return line;
        } catch (IOException e) {
            System.out.println("Reconnecting...");
        }
        return null;
    }
    public void sendLine(String input){
        try {
            String sendable = Session.getInstance().getKey() + " " + input + '\n';
            outToServer.writeBytes(sendable);
            //System.out.println("C:"+sendable);
        } catch (IOException e) {
            System.out.println("Couldn't send data, attempting reconnect.");
        }
    }
    public String dualLine(String input){
        sendLine(input);
        return readLine();
    }
    public void close(){
        try {
            clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
