package com.company;

import java.util.ArrayList;

/**
 * Created by Vaughn on 2-2-2018.
 */
public class User implements java.io.Serializable {
    private int id;
    private String userName;
    private String password;
    private boolean loggedIn;
    private ArrayList<Ticket> ownedTickets = new ArrayList<>();
    private String lastSession;
    private double balance;

    public User(int id, String userName, String password) {
        this.id = id;
        this.userName = userName;
        this.password = password;
    }

    public void setOwnedTickets(ArrayList<Ticket> ownedTickets) {
        this.ownedTickets = ownedTickets;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public int getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public ArrayList<Ticket> getOwnedTickets() {
        return ownedTickets;
    }

    public void addTicket(Ticket ticket){
        ownedTickets.add(ticket);
    }
    public double getBalance() {
        return balance;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public String getLastSession() {
        if(!isLoggedIn()){
            return "0";
        }
        return lastSession;
    }

    public void setLastSession(String lastSession) {
        this.lastSession = lastSession;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }
}
