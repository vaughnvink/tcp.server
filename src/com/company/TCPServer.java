package com.company;
/*
 * TCPDatanetworks - com.company
 * Created by Vaughn on 2-2-2018.
 */

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

class TCPServer {
    private int IDCounter = 0;
    private ArrayList<User> userList = Database.i().getUsers();
    TCPServer(){}


    void init() {
        String serverInput;
        String serverOutput;
        ServerSocket welcomeSocket = null;
        try {
            welcomeSocket = new ServerSocket(1244);
        } catch (IOException e) {
            e.printStackTrace();
        }

        while(true) {
            try {
                assert welcomeSocket != null;
                Socket connectionSocket = welcomeSocket.accept();

                BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
                DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());
                while ((serverInput = inFromClient.readLine()) != null) {
                    System.out.println("C:"+serverInput);
                    Database.i().log("C:"+serverInput + "\n");
                    serverOutput = lineHandler(serverInput);

                    System.out.println("S:"+serverOutput);
                    Database.i().log("C:"+serverOutput + "\n");
                    outToClient.writeBytes(serverOutput + '\n');
                    Database.i().saveData();
                }
                break;
            } catch (Exception e) {
                System.out.println("Client disconnected!");
            }
        }
    }
    /*
    public void start() {
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(1244);
        while(true) {
            Socket connectionSocket = serverSocket.accept();
            BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));

            DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());
            String line = inFromClient.readLine();
            System.out.println("C:"+line);
            clientSentence = line;
            //clientSentence = readLine();
            capitalizedSentence = lineHandler(clientSentence);
            outToClient.writeBytes(capitalizedSentence + '\n');
            System.out.println("S:"+capitalizedSentence);
            //sendLine(capitalizedSentence);
        }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/
    private boolean checkSession(String token){
        if(token.equals("0")){
            return false;
        }
        int id = findUserToken(token);
        if (id != -1){
            if(userList.get(id).getLastSession().equals(token)){
                return true;
            }
        }
        return false;
    }
    private String lineHandler(String line){
        String response;
        List<String> args = new ArrayList<>(Arrays.asList(line.split(" ")));
        int argSize = args.size();
        switch (args.get(1)){
            case "login":
                if(argSize == 4){
                    response = "210 " + loginUser(args.get(2),args.get(3));
                } else {
                    response = "400 Please enter a username and password";
                }
                break;
            case "register":
                if(argSize == 4){
                    response = "210 " +registerUser(args.get(2),args.get(3));
                } else {
                    response = "400 Please specify a username and password";
                }
                break;
            case "logout":
                if(argSize == 2){
                    if(checkSession(args.get(0))){
                        int id = findUserToken(args.get(0));
                        User user = userList.get(id);
                        user.setLastSession(null);
                        user.setLoggedIn(false);
                        userList.set(id,user);
                        response = "210 " + logoutUser(user.getUserName());
                    } else {
                        response = "430 Session token " + args.get(0) + " not valid.";
                    }
                } else {
                    response = "410 Entered too many arguments!";
                }
                break;
            case "balance":
                if(argSize == 2){
                    if(checkSession(args.get(0))){
                        response = "210 Balance: " + userList.get(findUserToken(args.get(0))).getBalance();
                    } else {
                        response = "430 Session token " + args.get(0) + " not valid.";
                    }
                } else {
                    response = "400 Entered too many arguments!";
                }
                break;
            case "deposit":
                try {
                    if(argSize == 3){
                        double value = Double.valueOf(args.get(2));
                        if(checkSession(args.get(0)) && value > 0){
                            userList.get(findUserToken(args.get(0))).setBalance(userList.get(findUserToken(args.get(0))).getBalance()+Double.valueOf(args.get(2)));
                            response = "210 Balance updated.";
                        } else {
                            response = "430 Session token " + args.get(0) + " not valid.";
                        }
                    } else {
                        response = "410 Please type the number you would like to add to your balance!";
                    }
                } catch(Exception e){
                    response = "500 invalid number entered!";
                }
                break;
            case "withdraw":
                try {
                    if(argSize == 3){
                        double value = Double.valueOf(args.get(2));

                        if(checkSession(args.get(0)) && value > 0){
                            int usr = findUserToken(args.get(0));
                            if(userList.get(usr).getBalance() <= value){
                                value = userList.get(usr).getBalance();
                            }
                            userList.get(usr).setBalance(userList.get(usr).getBalance()-value);
                            response = "210 Balance updated, " + value + " returned.";
                        } else {
                            response = "430 Session token " + args.get(0) + " not valid.";
                        }
                    } else {
                        response = "410 Please type the number you would like to add to your balance!";
                    }
                } catch(Exception e){
                    response = "500 invalid number entered!";
                }
                break;
            case "list":
                if(argSize == 2){
                    if(checkSession(args.get(0))){
                        response = "210 ---";
                        for (int i = 0; i < Database.i().getConcerts().size(); i++) {

                            response = response + " ID:" + i + " " + Database.i().getConcerts().get(i).toString() + " --- ";
                        }
                    } else {
                        response = "430 Session token " + args.get(0) + " not valid.";
                    }
                } else {
                    response = "410 Please type the number you would like to add to your balance!";
                }
                break;
            case "tickets":
                if(argSize == 2){
                    if(checkSession(args.get(0))){
                        response = "210 ---";
                        for (int i = 0; i < userList.get(findUserToken(args.get(0))).getOwnedTickets().size(); i++) {
                            Ticket con = userList.get(findUserToken(args.get(0))).getOwnedTickets().get(i);
                            response = response + con.getBarCode() + " " + con.getConcert().getName() + " --- ";
                        }
                    } else {
                        response = "430 Session token " + args.get(0) + " not valid.";
                    }
                } else {
                    response = "410 Please type the number you would like to add to your balance!";
                }
                break;
            case "help":
                response = "<> The Concert Server <> ---" +
                        "Supported commands: ---" +
                        "register <username> <password> - registers account ---" +
                        "login <username> <password> - logs into account ---" +
                        "logout - logs out of account ---" +
                        "list - shows current concerts ---" +
                        "balance - shows your credits balance ---" +
                        "deposit - adds funds to account ---" +
                        "withdraw - removes funds to account ---" +
                        "tickets - prints all owned tickets ---" +
                        "buy <concert ID> - buys a ticket to the concert ---"+
                        "help - displays all available commands";
                break;
            case "buy":
                try {
                    if(argSize == 3){
                        int value = Integer.valueOf(args.get(2));
                        if(checkSession(args.get(0)) && value >= 0){
                            if(Database.i().getConcerts().get(value).getTickets() > 0){
                                int usr = findUserToken(args.get(0));
                                if(userList.get(usr).getBalance() >= Database.i().getConcerts().get(value).getPrice()){
                                    userList.get(usr).setBalance(userList.get(usr).getBalance()-Database.i().getConcerts().get(value).getPrice());
                                    Ticket acquired = Database.i().getConcerts().get(value).generateTicket();
                                    userList.get(usr).addTicket(acquired);
                                    response = "210 Purchased ticket " + acquired.getBarCode() + " for: " + acquired.getConcert();
                                } else {
                                    response = "440 Not enough credits stored in account!";
                                }
                            } else {
                                response = "440 Concert sold out.";
                            }
                        } else {
                            response = "430 Session token " + args.get(0) + " not valid.";
                        }
                    } else {
                        response = "410 Please type the number you would like to add to your balance!";
                    }
                } catch(Exception e){
                    response = "500 invalid number entered!";
                }
                break;
            default:
                response = "401 Unknown command, type in 'help' for a list of usable commands.";
                break;
        }
        return response;
    }
    /*
    public String readLine(){
        try {
            String line = inFromClient.readLine();
            System.out.println("C:"+line);
            return line;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    public void sendLine(String input){
        try {
            outToClient.writeBytes(input + '\n');
            System.out.println("S:"+input);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/
    private boolean userExists(String userName){
        for (User anUserList : userList) {
            if (anUserList.getUserName().equals(userName)) {
                return true;
            }
        }
        return false;
    }
    private int findUser(String userName){
        for (int i = 0; i < userList.size(); i++) {
            if (userList.get(i).getUserName().equals(userName)){
                return i;
            }
        }
        return -1;
    }
    private int findUserToken(String token){
        for (int i = 0; i < userList.size(); i++) {
            if (userList.get(i).getLastSession().equals(token)){
                return i;
            }
        }
        return -1;
    }
    private String registerUser(String userName, String password){
        if(!userExists(userName)){
            userList.add(new User(++IDCounter,userName,password));
            return "Successfully registered user: "+ userName + "!";
        }
        return "User already exists!";
    }
    private String generateToken(){
        int randomNum = ThreadLocalRandom.current().nextInt(1, 1000000000 + 1);
        return Integer.toHexString(randomNum);
    }

    private String loginUser(String userName, String password){
        if(userExists(userName)){
            User user = userList.get(findUser(userName));
            if(user.getPassword().equals(password)){
                user.setLoggedIn(true);
                String token = generateToken() + userName;
                user.setLastSession(token);
                userList.set(findUser(userName),user);
                return "token>" + token;
            }
            return "Password is incorrect!";
        }
        return "Username not found!";
    }

    private String logoutUser(String userName){
        userList.get(findUser(userName)).setLoggedIn(false);
        return "Logged out!";
    }
}