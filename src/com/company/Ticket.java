package com.company;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Vaughn on 2-2-2018.
 */
public class Ticket implements java.io.Serializable {
    private Concert concert;
    private String barCode;
    public Ticket(Concert concert) {
        this.concert = concert;
    }

    public Concert getConcert() {
        return concert;
    }

    public String getBarCode() {
        if(barCode == null){
            this.barCode = "" + ThreadLocalRandom.current().nextInt(1, 1000000000 + 1);
        }
        return barCode;
    }
}
