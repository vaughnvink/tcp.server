package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ClientExec {
    public static void main(String[] args) {
        Session datagram = Session.getInstance();
        while (true){
            BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
            TCPClient client = new TCPClient();
            try {
                client.dualLine(inFromUser.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
            client.close();
        }
    }
}
