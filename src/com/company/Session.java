package com.company;

/*
 * Created by Vaughn on 4-2-2018.
 */
public class Session {
    private static Session ourInstance = new Session();

    public static Session getInstance() {
        return ourInstance;
    }

    private String key = "0";
    private String host = "192.168.2.11";
    private int port = 1244;

    public Session(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public Session() {
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
