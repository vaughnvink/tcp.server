package com.company;

import java.util.ArrayList;

/*
 * Created by Vaughn on 4-2-2018.
 */
public class Concert implements java.io.Serializable {
    private ArrayList<Artist> artists = new ArrayList<>();
    private int tickets;
    private double price;
    private String name;

    public Concert(ArrayList<Artist> artists, int tickets, double price, String name) {
        this.artists = artists;
        this.tickets = tickets;
        this.price = price;
        this.name = name;
    }
    public Ticket generateTicket(){
        if(tickets>0){
            tickets--;
            return new Ticket(this);
        }
        return null;
    }
    public void addArtist(Artist artist){
        artists.add(artist);
    }
    public String toString(){
        String returnable;
        returnable = name + " | Artist(s):";
        for (int i = 0; i < artists.size(); i++) {
            returnable = returnable + " " +  artists.get(i).getStageName();
        }
        returnable = returnable + " | Price: " + price + " | Tickets: " + tickets;
        return returnable;
    }
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public ArrayList<Artist> getArtists() {
        return artists;
    }

    public void setArtists(ArrayList<Artist> artists) {
        this.artists = artists;
    }

    public int getTickets() {
        return tickets;
    }

    public void setTickets(int tickets) {
        this.tickets = tickets;
    }

    public String getName() {
        return name;
    }
}
