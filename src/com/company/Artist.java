package com.company;

/**
 * Created by Vaughn on 4-2-2018.
 */
public class Artist implements java.io.Serializable {
    private String stageName;

    public Artist(String stageName) {
        this.stageName = stageName;
    }

    public String getStageName() {
        return stageName;
    }

    public void setStageName(String stageName) {
        this.stageName = stageName;
    }
}
