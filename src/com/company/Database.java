package com.company;

import java.io.*;
import java.util.ArrayList;

/*
 * Created by Vaughn on 4-2-2018.
 */
public class Database implements java.io.Serializable {
    private static Database ourInstance = new Database();

    public static Database i() {
        return ourInstance;
    }

    private ArrayList<User> users = new ArrayList<>();
    private ArrayList<Concert> concerts = new ArrayList<>();
    private ArrayList<Artist> artists = new ArrayList<>();

    private Database() {
        if(!loadData()){
            System.out.println("Error while loading database, will generate fake testing data...");
            generateTestingData();
        }
    }
    public void log(String str){
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("log.txt", true));
            writer.append(' ');
            writer.append(str);

            writer.close();
        } catch(IOException e){
            e.printStackTrace();
        }
    }
    public void saveData(){
        try {
            FileOutputStream fileOut = new FileOutputStream("save.txt");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(this);
            out.close();
            fileOut.close();
        } catch(IOException e){
            e.printStackTrace();
        }
    }
    public boolean loadData(){
        try {
            FileInputStream fis = new FileInputStream("save.txt");
            ObjectInputStream ois = new ObjectInputStream(fis);
            Database temp = (Database) ois.readObject();
            artists = temp.getArtists();
            concerts = temp.getConcerts();
            users = temp.getUsers();
            ois.close();
            fis.close();
            System.out.println("Loaded data!");
            return true;
        } catch(Exception e){
            System.out.println("Failed to load database!");
            //e.printStackTrace();
            return false;
        }
    }
    public void generateTestingData(){
        artists.add(new Artist("Billy Bob"));
        artists.add(new Artist("Mc. Scotsman"));
        artists.add(new Artist("De Frans"));
        artists.add(new Artist("Robbie Smailliw"));

        concerts.add(new Concert(artists,500,34.7,"The big four"));
        ArrayList<Artist> temp = new ArrayList<>();
        temp.add(artists.get(1));
        concerts.add(new Concert(temp,100,65.7,"Scottish Jazz"));
        System.out.println("Testing data added to database!");
    }
    public ArrayList<User> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<User> users) {
        this.users = users;
    }

    public ArrayList<Concert> getConcerts() {
        return concerts;
    }

    public void setConcerts(ArrayList<Concert> concerts) {
        this.concerts = concerts;
    }

    public ArrayList<Artist> getArtists() {
        return artists;
    }

    public void setArtists(ArrayList<Artist> artists) {
        this.artists = artists;
    }
}
